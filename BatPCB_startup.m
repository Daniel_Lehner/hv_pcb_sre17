%% Simulationtime
timestep = 0.01;

%% Battery (battery.x)
battery.temp.discharge = 100;
battery.temp.charge = 55;
battery.powerLimit = 80000;
battery.cell.U_PP.min = 0;
battery.cell.U_PP.max = 0;
battery.Ri = 500; 
battery.numberParallelCells = 6;
battery.totalCapacity = 0;
battery.startingCharge = 0;
battery.temp.startFan = 20;
battery.temp.cellCrit = 0;

%% CycleTime
ct.hundred = 0.1; % [s]
ct.ten = 0.01; % [s]
ct.five = 0.005; % [s]
ct.cyc = 10; % Anzahl Zyklen bis Error